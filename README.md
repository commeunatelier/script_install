# Script installation :


![Docker](https://www.docker.com/wp-content/uploads/2022/03/Moby-logo.png){width=70 height=(50} 
Docker
---
install curl `sudo apt install curl -y`
```
curl https://gitlab.com/commeunatelier/script_install/-/raw/main/docker_install.sh -o docker_install.sh && \
chmod +x docker_install.sh \
&& ./docker_install.sh
```
---

![EKSCTL](https://eksctl.io/img/eksctl-gopher.png){width=70 height=70} 
EKS CTL
---  
install curl `sudo apt install curl -y`
```
curl https://gitlab.com/commeunatelier/script_install/-/raw/main/ekscrl_install.sh -o ekscrl_install.sh && \
chmod +x ekscrl_install.sh  && \
./ekscrl_install.sh
```

---
![Jenkins](https://upload.wikimedia.org/wikipedia/commons/thumb/e/e9/Jenkins_logo.svg/1200px-Jenkins_logo.svg.png){width=70 height=(50} 
Jenkins 
---

> [Lien source Jenkins](https://www.jenkins.io/doc/book/installing/linux/#debianubuntu)

install curl `sudo apt install curl -y`
```
curl -fsSL https://pkg.jenkins.io/debian/jenkins.io-2023.key | sudo tee \
  /usr/share/keyrings/jenkins-keyring.asc > /dev/null
echo deb [signed-by=/usr/share/keyrings/jenkins-keyring.asc] \
  https://pkg.jenkins.io/debian binary/ | sudo tee \
  /etc/apt/sources.list.d/jenkins.list > /dev/null \
sudo apt-get update && \
sudo apt-get install jenkins
```
---

![KUBECTL](https://upload.wikimedia.org/wikipedia/commons/thumb/3/39/Kubernetes_logo_without_workmark.svg/2109px-Kubernetes_logo_without_workmark.svg.png){width=70 height=70} 
Kubectl 
---

> [Lien source kubeCTL](https://kubernetes.io/fr/docs/tasks/tools/install-kubectl/)

install curl `sudo apt install curl -y`
```
curl -LO https://dl.k8s.io/release/$(curl -Ls https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl && \
chmod +x ./kubectl && \
sudo mv ./kubectl /usr/local/bin/kubectl && kubectl version --client \
```
---

![Terraform](https://humancoders-formations.s3.amazonaws.com/uploads/course/logo/541/formation-terraform.png){width=70 height=(70}
Terraform
---

install curl `sudo apt install curl -y`
```
curl https://gitlab.com/commeunatelier/script_install/-/raw/main/terraform_install.sh -o terraform_install.sh && \
chmod +x terraform_install.sh && \
./terraform_install.sh
```
---
