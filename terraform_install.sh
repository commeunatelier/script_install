#!/bin/bash
## source https://developer.hashicorp.com/terraform/tutorials/aws-get-started/install-cli

#
COLOR='\033[0;35m'
YELLOW='\033[1;33m'
NOCOLOR='\033[0m'
echo -e "${COLOR}
################################################################################################################################
# TERRAFORM Install
################################################################################################################################${NOCOLOR} "


echo -e "${COLOR}
################################################################################################################################
Ensure that your system is up to date and you have installed the gnupg, software-properties-common, and curl packages installed.
You will use these packages to verify HashiCorp's GPG signature and install HashiCorp's Debian package repository.
################################################################################################################################
${NOCOLOR} "
sudo apt-get update && sudo apt-get install -y gnupg software-properties-common

echo -e "${COLOR}
################################################################################################################################
Install the HashiCorp GPG key.${NOCOLOR} "
echo ""

wget -O- https://apt.releases.hashicorp.com/gpg | \
gpg --dearmor | \
sudo tee /usr/share/keyrings/hashicorp-archive-keyring.gpg

echo -e "${COLOR}
################################################################################################################################
Verify the key's fingerprint.${NOCOLOR} "
echo ""

gpg --no-default-keyring \
--keyring /usr/share/keyrings/hashicorp-archive-keyring.gpg \
--fingerprint

echo -e "${COLOR}
################################################################################################################################
Add the official HashiCorp repository to your system. The lsb_release -cs command finds the distribution release codename for
your current system, such as buster, groovy, or sid.${NOCOLOR} "
echo ""

echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] \
https://apt.releases.hashicorp.com $(lsb_release -cs) main" | \
sudo tee /etc/apt/sources.list.d/hashicorp.list

echo -e "${COLOR}
################################################################################################################################
Download the package information from HashiCorp.${NOCOLOR} "
echo ""

sudo apt update

echo -e "${COLOR}
################################################################################################################################
Install Terraform from the new repository.${NOCOLOR} "
echo ""

sudo apt-get install terraform

echo -e "${COLOR}
################################################################################################################################
clean install${NOCOLOR} "
echo ""

sleep 5s
sudo apt autoremove --purge
echo -e "${COLOR}
################################################################################################################################
clean install${NOCOLOR}
Verify that the installation worked by opening a new terminal session and listing Terraform's available subcommands.clean install${NOCOLOR} "
echo ""
terraform
sleep 2s
rm terraform_install.sh
