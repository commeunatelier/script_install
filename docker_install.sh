#!/bin/bash
# COLOR
RED='\033[0;31m'
AUTRE='\033[0;36m'
YELLOW='\033[1;33m'
NOCOLOR='\033[0m'
echo -e "${AUTRE}
###############################################################################################################
# Docker install DEBIAN
###############################################################################################################${NOCOLOR} "
echo ""
for pkg in docker.io docker-doc docker-compose podman-docker containerd runc; do sudo apt-get remove $pkg; done
echo ""
echo -e "${RED}
#####################################      Remove older installation      ######################################${NOCOLOR} "
echo ""
sudo apt remove --purge docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin

echo -e "${YELLOW}
###############################################################################################################
####### update the apt package index and install packages to allow apt to use a repository over HTTPS : #######${NOCOLOR} "
echo ""
echo "create folder install_docker"
echo ""
mkdir ~/install_docker && cd $_  &&
sudo apt-get update && sudo apt-get curl sudo -y

echo -e "${AUTRE}
###############################################################################################################
############################# download ansd execute script get.docker.com : ###################################${NOCOLOR}"

curl -fsSL https://get.docker.com -o install-docker.sh
chmod +x install-docker.sh
sh install-docker.sh --channel stable &&

echo -e "${YELLOW}

###############################################################################################################
################################## Add your user to the docker group : ########################################${NOCOLOR} "
echo ""
echo "sudo usermod -aG docker $USER"

echo ""
sudo usermod -aG docker $USER
echo ""
echo -e "$AUTRE}
###################################### test command docker ps & images : ######################################${NOCOLOR}"
echo -e "${YELLOW}"
echo ""

echo ""
docker ps
echo ""
echo -e "${AUTRE}"
docker images

echo ""
echo -e "${NOCOLOR}"

sudo systemctl enable docker.service && sudo systemctl enable containerd.service
sudo apt autoremove --purge
echo ""
echo "cleaning install"
cd ~/
rm -R install-docker.sh
rm docker_install.sh
echo -e "${AUTRE}
###############################################################################################################

###########################################  Go to use Docker  ################################################

###############################################################################################################${NOCOLOR}"
echo ""
